#!/usr/bin/env python
# control.py

"""
Author : peters 

Revision History:
File created on 26 Aug 2010.
Adapted by super004 on 26 Jan 2017.

"""

import logging

################### Begin Class CO2DaSystem ###################

from da.baseclasses.dasystem import DaSystem

class CO2DaSystem(DaSystem):
    """ Information on the data assimilation system used. This is normally an rc-file with settings.
    """
    def validate(self):
        """ 
        validate the contents of the rc-file given a dictionary of required keys
        """

        needed_rc_items = ['obs.input.id',
                           'obs.input.nr',
                           'obs.spec.nr',
                           'obs.cat.nr',
                           'nffparameters',
                           'random.seed',
                           'emis.pparam',
                           'ff.covariance',
                           'obs.bgswitch',
                           'obs.background',
                           'emis.input.spatial',
                           'emis.input.tempobs',
                           'emis.input.tempprior',
                           'emis.paramfile',
                           'emis.paramfile2',
                           'run.emisflag',
                           'run.emisflagens',
                           'run.obsflag']


        for k, v in self.items():
            if v == 'True' : 
                self[k] = True
            if v == 'False': 
                self[k] = False

        for key in needed_rc_items:
            if key not in self:
                logging.warning('Missing a required value in rc-file : %s' % key)
        logging.debug('DA System Info settings have been validated succesfully')

################### End Class CO2DaSystem ###################


if __name__ == "__main__":
    pass
