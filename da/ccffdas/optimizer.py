#!/usr/bin/env python
# optimizer.py

"""
.. module:: optimizer
.. moduleauthor:: Wouter Peters 

Revision History:
File created on 28 Jul 2010.

"""

import os
import logging
import numpy as np
import numpy.linalg as la
import da.tools.io4 as io

identifier = 'Optimizer CO2'
version = '0.0'

from da.baseclasses.optimizer import Optimizer

################### Begin Class CO2Optimizer ###################

class CO2Optimizer(Optimizer):
    """
        This creates an instance of an optimization object. It handles the minimum least squares optimization
        of the state vector given a set of sample objects. Two routines will be implemented: one where the optimization
        is sequential and one where it is the equivalent matrix solution. The choice can be made based on considerations of speed
        and efficiency.
    """
    
    def setup(self, dims):
        self.nlag = dims[0]
        self.nmembers = dims[1]
        self.nparams = dims[2]
        self.nobs = dims[3]
        self.nrloc = dims[4]
        self.nrspc = dims[5]
        self.inputdir = dims[6]
        #self.specfile = dims[7]
        self.create_matrices()

    def set_localization(self, loctype='None'):
        """ determine which localization to use """

        if loctype == 'CT2007':
            self.localization = True
            self.localizetype = 'CT2007'
            #T-test values for two-tailed student's T-test using 95% confidence interval for some options of nmembers
            if self.nmembers == 50:
                self.tvalue = 2.0086
            elif self.nmembers == 100:
                self.tvalue = 1.9840
            elif self.nmembers == 150:
                self.tvalue = 1.97591
            elif self.nmembers == 200:
                self.tvalue = 1.9719    
            else: self.tvalue = 0 
        elif loctype == 'multitracer':
            self.localization = True
            self.localizetype = 'multitracer'
        elif loctype == 'multitracer2':
            self.localization = True
            self.localizetype = 'multitracer2'
        else:
            self.localization = False
            self.localizetype = 'None'
    
        logging.info("Current localization option is set to %s" % self.localizetype)

    def localize(self, n):
        """ localize the Kalman Gain matrix """
        import numpy as np

        if not self.localization: 
            return 
        if self.localizetype == 'CT2007':
            count_localized = 0
            for r in range(self.nlag * self.nparams):
                corr = np.corrcoef(self.HX_prime[n, :], self.X_prime[r, :].squeeze())[0, 1]
                prob = corr / np.sqrt((1.000000001 - corr ** 2) / (self.nmembers - 2))
                if abs(prob) < self.tvalue:
                    self.KG[r] = 0.0
                    count_localized = count_localized + 1
            logging.debug('Localized observation %i, %i%% of values set to 0' % (self.obs_ids[n],count_localized*100/(self.nlag * self.nparams)))
                                
