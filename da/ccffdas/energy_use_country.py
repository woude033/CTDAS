energy_use_per_country = {
'AUS': {'Public power': 161.01,
  'Industry': 158.02,
  'Other stationary combustion': 120.04,
  'Road transport': 150.8,
  'Shipping': 0.14},
 'BEL': {'Public power': 254.82,
  'Industry': 203.68,
  'Other stationary combustion': 383.16,
  'Road transport': 172.455,
  'Shipping': 5.63},
 'CZE': {'Public power': 582.9,
  'Industry': 140.13,
  'Other stationary combustion': 178.46,
  'Road transport': 121.935,
  'Shipping': 0.17},
 'FRA': {'Public power': 585.52,
  'Industry': 731.9,
  'Other stationary combustion': 1262.1,
  'Road transport': 845.07,
  'Shipping': 20.0},
 'DEU': {'Public power': 3515.68,
  'Industry': 1516.34,
  'Other stationary combustion': 2069.16,
  'Road transport': 1066.98,
  'Shipping': 26.11},
 'LUX': {'Public power': 3.75,
  'Industry': 15.23,
  'Other stationary combustion': 25.22,
  'Road transport': 37.045,
  'Shipping': 0.01},
 'NED': {'Public power': 868.96,
  'Industry': 427.43,
  'Other stationary combustion': 599.52,
  'Road transport': 199.74,
  'Shipping': 13.93},
 'POL': {'Public power': 1702.42,
  'Industry': 338.92,
  'Other stationary combustion': 618.77,
  'Road transport': 362.94,
  'Shipping': 0.29},
 'CHE': {'Public power': 43.41,
  'Industry': 66.5,
  'Other stationary combustion': 194.77,
  'Road transport': 100.28,
  'Shipping': 1.54},
 'GBR': {'Public power': 1716.77,
  'Industry': 620.58,
  'Other stationary combustion': 1485.23,
  'Road transport': 787.845,
  'Shipping': 72.39}}
