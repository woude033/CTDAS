# CarbonTracker Data Assimilation Shell (CTDAS) Copyright (C) 2017,2018,2019 Wouter Peters. 
# Users are recommended to contact the developers (wouter.peters@wur.nl) to receive
# updates of the code. See also: http://www.carbontracker.eu. 
#
# This program is free software: you can redistribute it and/or modify it under the
# terms of the GNU General Public License as published by the Free Software Foundation, 
# version 3. This program is distributed in the hope that it will be useful, but 
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS 
# FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details. 
#
# You should have received a copy of the GNU General Public License along with this 
# program. If not, see <http://www.gnu.org/licenses/>. 

# CTDAS code can be downloaded from https://git.wur.nl/woude033/test_CTDAS.git

#!/bin/bash

##### This script:
# Use to set up a new ctdas run
# either clone the current branch or 
# Create a new branch, based on the current branch

# Usage: 
## Options: 
#   -n: no new branch: clone the current branch
#   -M: Master: Create a new branchable branch (i.e. do NOT remove the start_ctdas.sh script

# NOTE THAT THIS SCRIPT DOES NOT CLONE FROM THE GITLAB
# But rather from the current directory.
# This means that pushing will push to the current directory,
# And that a push to the gitlab should be in two steps: First to this directory and then to the gitlab (Or set the upstream to gitlab)


set -e

usage="$basename "$0" [arg1] [arg2] [arg3] [-h] -- script to start a new CTDAS run  

where:
     arg1: directory to run from (i.e., /scratch/"$USER"/)
     arg2: project name (i.e, test_ctdas)
     arg3: branch name
     -h  shows this help text
     -M: create new branch from which clones can be made (new branch is a Master)
     -n: clone new branch from current branch
     ! A new folder will then be created and populated:

     /scratch/"$USER"/test_ctdas/

    "

rootdir=$1/$2
rundir=$1/$2/exec
branchname=$3

delete=1
while getopts ':nMhs:' option; do
  case "$option" in
    M) delete=0
       echo '          '
       echo New branch is a branch from which new clones can be made, not removing the start_ctdas.sh script
       echo '          '
       rootdir=$2/$3
       echo $rootdir
       rundir=$2/$3/exec
       echo $rundir
       branchname=$4
       ;;

    n) echo '          '
       echo Cloning from current branch
       echo '          '
       rootdir=$2/$3
       echo $rootdir
       rundir=$2/$3/exec
       echo $rundir

       echo "New project to be started in folder $2"
       echo "               ...........with name $3"
       
       if [ -d "$rootdir" ]; then
           echo "Directory already exists, please remove before running $0"
           exit 1
       fi
       
       mkdir -p ${rundir}
       git clone -q ./ ${rundir}/
       cd ${rundir}
       
       echo "Creating jb file, py file, and rc-file"
       cp templates/template.jb ${branchname}.jb
       cp templates/template.py ${branchname}.py
       cp templates/template.rc ${branchname}.rc
       rm -f start_ctdas.sh       
       chmod u+x ${branchname}.jb
       
       echo ""
       echo "************* NOW USE ****************"
       ls -lta ${branchname}.*
       echo "**************************************"
       echo ""
       pwd
       exit
       ;; 
    h) echo "$usage"
       exit
       ;;
    :) printf "missing argument for -%s\n" "$OPTARG" >&2
       echo "$usage" >&2
       exit 1
       ;;
   \?) printf "illegal option: -%s\n" "$OPTARG" >&2
       echo "$usage" >&2
       exit 1
       ;;
  esac
done

echo "New project to be started in folder $2"
echo "               ...........with name $3"

if [ -d "$rootdir" ]; then
    echo "Directory already exists, please remove before running $0"
    exit 1
fi

mkdir -p ${rundir}
git branch ${branchname}
git checkout ${branchname}
git clone -q ./ ${rundir}/
git checkout @{-1}
cd ${rundir}

echo "Creating jb file, py file, and rc-file"
cp templates/template.jb ${branchname}.jb
cp templates/template.py ${branchname}.py
cp templates/template.rc ${branchname}.rc
if [ "$delete"=1 ]; then
    rm -f start_ctdas.sh
fi

sed -e "s/template/$2/g" templates/template.jb > $2.jb
sed -e "s,template,${rootdir},g" templates/template.rc > $2.rc

chmod u+x ${branchname}.jb

echo ""
echo "************* NOW USE ****************"
ls -lta ${branchname}.*
echo "**************************************"
echo ""
pwd

